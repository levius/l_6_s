
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Server implements Runnable {
    private ServerSocketChannel serverSocket;
    private int i = 8900;
    String FileForLoad;
    Vector<Picture> vector;
    LinkedList<Handler> clients = new LinkedList<>();

    int size;
    Date initDate;

    Server(String file, int i) {
        FileForLoad = file;
        Collection();
        this.i = i;
    }
    Server(int i) {
        this.i = i;
        initDate = new Date();
        vector = new Vector<>();
    }

    Server(String file) {
        FileForLoad = file;
        Collection();
    }

    Server() {
        initDate = new Date();
        vector = new Vector<>();
    }

    public void run() {

        SocketAddress socketAddress = new InetSocketAddress("helios.cs.ifmo.ru", i);
        System.out.println("Попытка запустить сервер...");
        try {
            serverSocket = ServerSocketChannel.open();
            System.out.println("opened");
            serverSocket.bind(socketAddress);
            System.out.println("Порт: " + serverSocket.socket().getLocalPort());
            System.out.println(socketAddress);
        } catch (IOException e) {
            System.out.println("Что-то пошло не так");
            System.exit(-1);
        }
        while (serverSocket.isOpen()) {
            SocketChannel clientChannel = waitConnection();
            if (clientChannel == null) break;
            Handler handler = new Handler(clientChannel, this);
            clients.add(handler);
            handler.go();

        }
    }

    private void Collection() {
        initDate = new Date();
        vector = new Vector<>();
        import_file(FileForLoad);
    }

    protected void import_file(String filedata) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = documentBuilder.parse(new ByteArrayInputStream(filedata.getBytes(StandardCharsets.UTF_8)));

            NodeList ListofPictures = doc.getElementsByTagName("picture");

            for (int i = 0; i < ListofPictures.getLength(); i++) {
                Element pic = (Element) ListofPictures.item(i);
                NodeList ListNames = pic.getElementsByTagName("name");
                NodeList ListCreators = pic.getElementsByTagName("creator");
                Element creator = (Element) ListCreators.item(0);
                vector.add(new Picture(new Creator(ListCreators.item(0).getFirstChild().getNodeValue()), ListNames.item(0).getFirstChild().getNodeName()));
            }
            size = vector.size();
            System.out.println("Файл успешно загружен");
        } catch (ParserConfigurationException e) {
            System.out.println("Ошибка структуры файлы");
        } catch (SAXException | IOException l) {
            System.out.println("Ошибка загрузки файла");
        }
    }

    private SocketChannel waitConnection() {
        try {
            SocketChannel client;
            System.out.println("Ждём подключения");
            client = serverSocket.accept();
            System.out.println("Произошло подключение.");
            return client;
        } catch (ClosedByInterruptException e) {
            System.out.println("Сервер выключается");
            try {
                serverSocket.close();
                clients.stream().forEach(c -> c.thread.interrupt());
            } catch (IOException ex) {
                System.out.println("Не удалось закрыть сетевой канал.");
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Что-то не так");
            return null;
        }
    }
}
