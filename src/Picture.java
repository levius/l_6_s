import java.io.Serializable;

public class Picture implements Comparable<Picture>, Serializable {
    protected String name;
    protected Creator creator;
    protected int artCOST;
    Picture( Creator _creator, String _name) {
        artCOST = 0;
        creator = _creator ;
        name = _name;
        artCOST += this.getCreator().length()*6;
    }
    Picture(String _name) {
        artCOST = 0;
        creator = new Creator("Картина неизвестного художника");
        name = _name;
        artCOST += this.getCreator().length()*6;
    }
    Picture( Creator _creator) {
        artCOST = 0;
        creator = _creator ;
        name = "У этой картины нет названия";
        artCOST += this.getCreator().length()*6;
    }
    Picture(){
        name = "У этой картины нет названия";
        creator = new Creator("Картина неизвестного художника");
    }


    public String getCreator() {
        return creator.getCreatorSurname(creator);
    }
    public String getName() { return name;
    }
    @Override
    public String toString() {
        Sign data = new Sign(this);
        return "\n Под картиной надпись:\n"+data.toString();
    }
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.getCreator() == null) ? 0 : this.getCreator().length());
        return result;
    }
    @Override
    public int compareTo(Picture p) {
        if (this.getName().length() > p.getName().length()) {
            return 1;
        }
        else if (this.getName().length() == p.getName().length()) {
            return 0;
        }
        else {
            return -1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (!(obj.getClass() == this.getClass())){
            return false;
        }
        Picture pic = (Picture) obj;
        if (this.getCreator().equals(pic.getCreator()) && this.getName().equals(pic.getName())){
            return true ;
        }
        return false;
    }
}
