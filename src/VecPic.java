//import java.util.Collections;
//import java.util.Date;
//import java.util.Vector;
//import org.w3c.dom.Document;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import java.io.*;
//import java.util.Vector;
//import org.w3c.dom.Element;
//import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;
//import javax.xml.parsers.ParserConfigurationException;
//import java.nio.charset.StandardCharsets;
//
//public class VecPic {
//
//    private static Vector<Picture> collection = new Vector<>();
//    static int k = 0 ;
//
//    private static Date createdDate = new Date();
//    private static boolean edited = false;
//
//
//
//
//    /**
//     * Добавляет картину в коллекцию
//     * @param picture картина, которую надо добавить
//     */
//    public static void add(Picture picture) {
//        //System.out.println("сейчас добавлю");
//        collection.add(picture);
//        edited = true;
//    }
//
//
//    /**
//     *<p>Сохраняет коллекцию в файл</p>
//     */
////    public static void save(PrintWriter writer)  {
////        writer.write("<?xml version=\"1.0\"?>\n");
////        writer.write("<PIC>\n");
////        writer.write("  <timestamp>" + getCreatedDate().getTime() + "</timestamp>\n");
////        for (Picture picture : getCollection()) {
////            writer.write("  <picture>\n");
////            writer.write("    <name>" + picture.getName() + "</name>\n");
////            writer.write("    <creator>" + picture.getCreator() + "</creator>\n" );
////            writer.write("  </picture>\n");
////        }
////        writer.write("</PIC>\n");
////        writer.flush();
////    }
//
//    /**
//     * <p>Добавляет в коллекцию содержимое файла</p>
//     * throws NullPointerException,IllegalArgumentException,SAXException,ParserConfigurationException
//     */
//    public void importing(String filename)throws IOException{
//            String _filename = filename;
//            System.out.println(_filename);
//            k =0;
//            InputStreamReader reader = new InputStreamReader(new FileInputStream(_filename));
////            line = new String();
//            StringBuilder builder = new StringBuilder();
//            try {
//                while (k != -1) {
//                    k = reader.read();
//                    char c  = (char) k;
////                    line += c;
//                    builder.append(c);
//                }
//                builder.deleteCharAt(builder.length() - 1);
////                line = line.substring(0, line.length()-1);
//
////                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
////
////                Document doc = documentBuilder.parse(new ByteArrayInputStream(builder.toString().getBytes(StandardCharsets.UTF_8)));
////
////                NodeList ListofPictures = doc.getElementsByTagName("picture");
////
////                for (int i = 0; i < ListofPictures.getLength(); i++) {
////                    Element pic = (Element) ListofPictures.item(i);
////                    NodeList ListNames = pic.getElementsByTagName("name");
////                    NodeList ListCreators = pic.getElementsByTagName("creator");
////                    Element creator = (Element) ListCreators.item(0);
////                    VecPic.add(new Picture(new Creator(ListCreators.item(0).getFirstChild().getNodeValue()), ListNames.item(0).getFirstChild().getNodeName()));
////                }
////                System.out.println("Файл успешно загружен");
////                reader.close();
////            }
////            catch(ParserConfigurationException e){
////                System.out.println("Ошибка структуры файлы");
////                System.exit(-1);
////            }
////            catch(SAXException|IOException l){
////                System.out.println("Ошибка загрузки файла");
////                System.exit(-1);
////            }
////        }
//
//
//
//
//
//    /**
//     * Удаляет существо из коллекции
//     * @param picture существо, которое надо удалить
//     * @return true, если удаление произошло
//     */
//    public boolean remove(Picture picture) {
//        for (Picture img : getCollection()) {
//             if (img.equals(picture)) {
//             getCollection().remove(img);
//             return true;
//             }
//        }
//        return false;
//    }
//
//
//
//    /**
//     * <p>список и описание команд</p>
//     */
//    public static void help(){
//        System.out.println("Каждая команда должна заканчиваться точкой с запятой");
//        System.out.println("Не используйте символ точка с запятой в именах");
//        System.out.println("insert int index {element} - команда добавления нового элемента в заданную позицию.");
//        System.out.println("Пример : insert  2 {\"name\":\"sobaka\",\"creator\":\"drawer\"}");
//        System.out.println("show - вывод элементов в нашей коллекции");
//        System.out.println("import path- добавить в коллекцию все данные из файла");
//        System.out.println("info - информация о коллекции, ее тип, дата создания и кол-во элементов");
//        System.out.println("remove {element} - удалить элемент коллекции по его значению. ");
//        System.out.println("Пример : remove {\"name\":\"SEA\"}");
//        System.out.println("add_if_min - добавляет новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции.");
//        System.out.println(" Пример: add_if_min {\"name\":\"BOY\", \"surname\":\"CHIK\"}");
//        System.out.println("add {element}- добавляет новый элемент в коллекцию. ");
//        System.out.println(" Пример: add {\"name\":\"BOY\", \"surname\":\"CHIK\"}");
//        System.out.println("exit - закрывает программу ");
//    }
//
//
//    /**
//     * <p>Добавляет новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции</p>
//     * @param picture Human
//     * throws NullPointerException,IllegalArgumentException
//     */
////    public  void add_if_min(Picture picture){
////        try {
////                if (picture.compareTo(Collections.min(collection)) < 0) {   //VOPROSIK!!!
////                    collection.add(picture);
////                    System.out.println("Картина успешно добавлена");
////                    edited = true;
////                }
////                else System.out.println("Картина не добавлена");
////        }
////        catch(NullPointerException|IllegalArgumentException e){
////            System.out.println("Неверный ввод");
////        }
////    }
//
//
//
//    /**
//     * <p>Добавляет элемент в коллекцию</p>
//     * @param index
//     * @param picture
//     * @throws NullPointerException
//     */
//    public static void insert(int index, Picture picture){
//        try {
//          collection.insertElementAt(picture, index);
//        }
//        catch (NullPointerException|StringIndexOutOfBoundsException|IllegalArgumentException e){
//            System.out.println("Неверный ввод");
//        }
//    }
//
//
//
//    /**
//     * @return коллекцию картин
//     */
//    public static Vector<Picture> getCollection() {
//        return collection;
//    }
//
//
//
//    public static int getSize() {
//        return collection.size();
//    }
//
//
//
//    public static Date getCreatedDate() {
//        return createdDate;
//    }
//
//
//
//
//    /**
//     * @return Возвращает информацию о коллекции
//     */
//    public String info () {
//        return "Коллекция содержит картины типа " + collection.getClass().getName() + ",\n" +
//                "создана " + createdDate + ",\n" +
//                "содержит " + collection.size() + " картин";
//    }
//}
//
