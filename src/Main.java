
import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    static Server server;

    public static void main(String[] args) {

        if (args.length > 1) {
            try {
                server = new Server(new String(args[0]), Integer.parseInt(args[1]));
            } catch (NumberFormatException e) {
                System.out.println("Номер порта задан неверно");
                server = new Server(new String(args[1]), Integer.parseInt(args[0]));
            }
        } else if (args.length == 1) {
            try {
                {
                    server = new Server(Integer.parseInt(args[0]));
                }
            } catch (NumberFormatException e) {
                server = new Server(new String(args[0]));
            }
        } else server = new Server();
        Thread serverThreat = new Thread(server);
        serverThreat.start();
        boolean exit = false;
        Scanner scanner = new Scanner(System.in);
        String s;
        try {
            while (!exit) {
                s = scanner.next();
                exit = s.equals("exit");
                if (s.equals("show")) {
                    server.clients.stream().forEach(c -> System.out.println(c.id));
                }
            }
            serverThreat.interrupt();
        } catch (NoSuchElementException e) {
            serverThreat.interrupt();
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                serverThreat.interrupt();
                finish();
            } catch (Exception e) {
                System.err.println("Данные не сохранены");
            }
        }));

    }

    private static void finish() {
        File savefile = new File("LP_SAVE.xml"); //адрес с сервера
        try {
            if (!savefile.exists()) {
                System.out.println("Файла не существует, создадим новый");
                savefile.createNewFile();
            }
            if (!savefile.isFile()) System.out.println("это не файл");
            if (!savefile.canWrite()) {
                System.out.println("Доступ для записи в файл запрещен");
            } else {
                FileOutputStream fl = new FileOutputStream(savefile);
                PrintWriter writer = new PrintWriter(fl);
                writer.write("<?xml version=\"1.0\"?>\n");
                writer.write("<PIC>\n");//мб нужна строчка которую  убрал
                for (Picture picture : server.vector) {
                    writer.write("  <picture>\n");
                    writer.write("    <name>" + picture.getName() + "</name>\n");
                    writer.write("    <creator>" + picture.getCreator() + "</creator>\n");
                    writer.write("  </picture>\n");
                }
                writer.write("</PIC>\n");
                writer.flush();
                writer.close();
                System.out.println("\nКоллекция сохранена в файл");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}