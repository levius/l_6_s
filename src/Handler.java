
import java.io.*;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.*;

import java.util.stream.Collectors;

class Handler {
    private SocketChannel socketChannel;
    Thread thread;
    private static int name = 0;
    int id;
    private Socket clientSocket;
    private ObjectInputStream reader;
    private ObjectOutputStream writer;
    private Server server;

    Handler(SocketChannel socketChannel, Server server) {
        this.server = server;
        this.id = ++name;
        this.socketChannel = socketChannel;
        this.clientSocket = socketChannel.socket();
        try {

            InputStream inputClientStream = clientSocket.getInputStream();
            OutputStream outClientStream = clientSocket.getOutputStream();
//            DataInputStream dout = new DataInputStream(reader);
            writer = new ObjectOutputStream(outClientStream);
            reader = new ObjectInputStream(inputClientStream);
            System.out.println("Клиент номер " + id + " подключился к серверу");
        } catch (IOException e) {
            System.out.println("Поток ввода не получен");
            System.exit(0);
        }
    }

    private void serveClient() {
        while (!clientSocket.isClosed()) {

            try {
                sendMessage("Введите команду");
                String Command = getMessage();
                System.out.println(Command);

                switch (Command) {
                    case "remove": {
                        Picture picture = (Picture) reader.readObject();
                        if (!this.server.vector.contains(picture)) sendMessage("Таких элементов нет");
                        else {
                            //Iterator<Picture> iter = server.vector.iterator();
                            //while (iter.hasNext()) {
//                                Picture pic = iter.next();
//                                if (pic.equals(picture)) {
//                                    iter.remove();
//                                }
//                            }
                            server.vector.stream().filter(t -> t.equals(picture)).forEach(t -> server.vector.remove(t));

                            sendMessage("Элемент удален");
                        }
                        break;
                    }
                    case "add": {
                        Picture h = (Picture) reader.readObject();
                        if (h == null) break;
//                                Server.vector.entrySet().stream().filter(t -> t.getValue().compareTo(h) < 0).forEach(t -> Server.tree.remove(t.getKey()));
                        this.server.vector.add(h);
                        sendMessage("Картина успешно добавлена");
                        break;
                    }
                    case "add_if_min": {
                        Picture pic = (Picture) reader.readObject();
                        if (pic == null) break;
                        try {
                            if (pic.compareTo(Collections.min(this.server.vector)) < 0) {   //VOPROSIK!!!
                                this.server.vector.add(pic);
                                sendMessage("Картина успешно добавлена");
//                                        edited = true;
                            } else sendMessage("Картина не добавлена");
                        } catch (NullPointerException | IllegalArgumentException e) {
                            sendMessage("Неверный ввод");
                        }
                        break;
                    }
                    case "show": {

                            sendObj(this.server.vector);

                        break;
                    }
                    case "info": {
                        sendMessage("Коллекция имеет тип VECTOR и содержит элементы типа PICTURE\n" +
                                "инициализировано " + this.server.initDate + "\n" +
                                "Коллекция содержит " + this.server.vector.size() + " элементов");
                        break;
                    }
                    case "import": {
                        System.out.println(1);
                        String file = getMessage();
                        int i;
                        System.out.println(file);
                        if (file != null) {
                            i = this.server.size;
                            this.server.import_file(file);
                            if (i != -1) {
                            }
                            //sendMessage("Коллекция загружена из файла, не инициализировано " + i + " элементов");
                            else
                                sendMessage("Загрузка коллекции из файла не удалась, файл не был найден или к нему отсутствует доступ");
                        } else {
                            sendMessage("Извините, файл не был задан(((((((");
                        }
                        break;
                    }
                    case "exit": {
                        sendMessage("До свидания!");
                        close();
                        break;
                    }
                    case "help": {
                        sendMessage("Доступные команды:\n" +
                                "insert {int index} {element}: добавить новый элемент с заданным индексом\n" +
                                "show: вывести в стандартный поток вывода все элементы коллекции в строковом представлении\n" +
                                "info: вывести в стандартный поток вывода информацию о коллекции (тип, дата инициализации, количество элементов и т.д.)\n" +
                                "add {element}: добавить в коллекцию картину\n" +
                                "add_if_min {element}: добавить в коллекцию картину если она меньше всех других\n" +
                                "remove {String key}: удалить элемент из коллекции по его значению\n" +
                                "import: path загрузить картины из файла в коллекцию\n" +
                                "exit: выйти из приложения, сихранив данные в файл");
                        break;
                    }
                    case "insert": {
                        String s = getMessage();
                        sendMessage(s);
                        sendMessage("asd");
                        int index = Integer.parseInt(s);
                        Picture Active = (Picture) reader.readObject();
                        if (Active == null) break;
                        if (index < this.server.vector.size() - 1) {
                            this.server.vector.add(index, Active);
                            sendMessage("Элемент добавлен");
                            break;
                        } else {
                            sendMessage("Слишком большой индекс");
                            break;
                        }
                    }

                    // sendMessage("Некорректный индекс, должен быть индекс число"); break;
                    default:
                        sendMessage("Такой команды нет");
                        break;
                }


            } catch (NullPointerException | IOException | ClassNotFoundException e) {
                e.printStackTrace();
                close();
                break;
            }
        }
    }

    private void close() {

        try {
            clientSocket.close();
            writer.close();
            reader.close();
            thread.interrupt();
            socketChannel.close();
            this.server.clients.remove(this);
            System.out.println("Обслуживание клиента " + id + " завершено");
        } catch (IOException e) {
            System.out.println("не удалось прервать обслуживание клиента");
        }
    }

    private void sendMessage(String s) {
        try {
            writer.writeUTF(s);
            writer.flush();
        } catch (IOException e) {
            System.out.println("Отправка не удалась");
        }
    }

    private void sendObj(Object o) {
        try {
            writer.writeObject(o);
            writer.flush();
        } catch (IOException e) {
            System.out.println("Отправка не удалась");
        }

    }

    private String getMessage() {
        try {
            String s = reader.readUTF();
            return s;
        } catch (Exception e) {
            System.out.println("Команда не была передана");
            return null;
        }

    }

    void go() {
        thread = new Thread(this::serveClient);
        thread.start();
    }
}
